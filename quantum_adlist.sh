#!/bin/bash
#############################################################
#############################################################
##                                                         ##
##      Thank you for choosing The Quantum Ad-List!        ##
##             We hope you will be satisfied!              ##
##              ---------------------------                ##
##                 By The Quantum Alpha                    ##
##                                                         ##
##         MeWe: https://mewe.com/join/TechnoChat          ##
##                                                         ##
##                                                         ##
##   You may please run this script with root privilege!   ##
##                                                         ##
#############################################################
#############################################################

check_root() {
    if [ "$EUID" -ne 0 ]; then
        echo "This script must be run as root."
        sudo "$0" "$@"
        exit 1
    fi
}

install_quantum_ad_list() {
    read -p "Do you want to make a backup before proceeding? (y/n): " backup_choice
    if [ "$backup_choice" == "y" ]; then
        cp /etc/hosts /etc/hosts.bak
        echo "Backup created at /etc/hosts.bak"
    fi

    echo "Downloading the Quantum Ad-List..."
    curl -s https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/-/raw/master/Hosts/The_Quantum_Ad-List_1.txt > /tmp/adlist1.txt
    curl -s https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/-/raw/master/Hosts/The_Quantum_Ad-List_2.txt > /tmp/adlist2.txt

    cat /tmp/adlist1.txt /tmp/adlist2.txt > /tmp/adlist.txt

    echo "Writing to /etc/hosts..."
    HOSTNAME=$(hostname)
    echo -e "# Quantum Ad-List - Don't remove this line\n127.0.0.1       localhost ${HOSTNAME}\n127.0.0.1       localhost.localdomain\n127.0.0.1       local\n255.255.255.255 broadcasthost\n::1             localhost ${HOSTNAME}\n::1             ip6-localhost ip6-loopback\nfe80::1%lo0     localhost\nfe00::0         ip6-localnet\nff00::0         ip6-mcastprefix\nff02::1         ip6-allnodes\nff02::2         ip6-allrouters\nff02::3         ip6-allhosts\n0.0.0.0         0.0.0.0" > /etc/hosts
    cat /tmp/adlist.txt >> /etc/hosts

    domain_count=$(wc -l < /tmp/adlist.txt)
    echo "Installation completed. $domain_count domains were added. You may need to restart your network services."
}

update_quantum_ad_list() {
    if ! grep -q "# Quantum Ad-List - Don't remove this line" /etc/hosts; then
        echo "The Quantum Ad-List is not installed. Please install it first."
        return
    fi

    echo "Downloading the Quantum Ad-List..."
    curl -s https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/-/raw/master/Hosts/The_Quantum_Ad-List_1.txt > /tmp/adlist1.txt
    curl -s https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/-/raw/master/Hosts/The_Quantum_Ad-List_2.txt > /tmp/adlist2.txt

    cat /tmp/adlist1.txt /tmp/adlist2.txt > /tmp/adlist.txt

    echo "Updating /etc/hosts..."
    HOSTNAME=$(hostname)
    header=$(grep -E "^#|^127\.0\.0\.1|^255\.255\.255\.255|^::1|^fe80::1%lo0|^fe00::0|^ff00::0|^ff02::1|^ff02::2|^ff02::3|^0\.0\.0\.0" /etc/hosts)
    echo -e "$header" > /etc/hosts

    new_domains=$(comm -23 <(sort /tmp/adlist.txt) <(sort /etc/hosts))
    removed_domains=$(comm -13 <(sort /etc/hosts) <(sort /tmp/adlist.txt))

    echo "$new_domains" >> /etc/hosts
    echo "$removed_domains" | xargs -I {} sed -i "/{}/d" /etc/hosts

    new_count=$(wc -l <<< "$new_domains")
    removed_count=$(wc -l <<< "$removed_domains")
    echo "Update completed. $new_count domains were added and $removed_count domains were removed. You may need to restart your network services."
}

restore_backup() {
    if [ -f /etc/hosts.bak ]; then
        cp /etc/hosts.bak /etc/hosts
        echo "Backup restored successfully."
    else
        echo "Failed to restore from backup. No backup was created."
    fi
}

whitelist_domains() {
    read -p "Enter the URLs to whitelist, separated by commas: " urls
    IFS=',' read -ra url_array <<< "$urls"

    for url in "${url_array[@]}"; do
        sed -i "/0\.0\.0\.0 $url/d" /etc/hosts
    done

    domain_count=$(grep -c "0\.0\.0\.0" /etc/hosts)
    echo "Domains whitelisted. There are now $domain_count domains in the hosts file."
    echo "Please send the domains to whitelist to our GitLab page at https://gitlab.com/The_Quantum_Alpha/the-quantum-ad-list/-/issues"
}

check_root

echo "Please select an option:"
echo "1. Install The Quantum Ad-List on system's Hosts file"
echo "2. Update The Hosts file"
echo "3. Restore from backup"
echo "4. Whitelist a domain from hosts file"
read -p "Enter your choice: " choice

case $choice in
    1)
        install_quantum_ad_list
        ;;
    2)
        update_quantum_ad_list
        ;;
    3)
        restore_backup
        ;;
    4)
        whitelist_domains
        ;;
    *)
        echo "Invalid choice. Please run the script again and select a valid option."
        ;;
esac
